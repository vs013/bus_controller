const NODE_ENV = process.env.NODE_ENV || 'development';
const webpack = require('webpack');
const sourcePath = './app/static/src/';
const outputPath = '/app/static/js';

module.exports = {
  entry : {
    base: sourcePath + 'base.js',
    project: sourcePath + 'project.js',
  },
  output: {
    publicPath: '/static/js/',
    path: __dirname + outputPath,
    filename: '[name].js',
    library: '[name]',
  },

  watch: NODE_ENV == 'development',

  watchOptions: {
    aggregateTimeout: 100,
  },

  devtool: NODE_ENV == 'development' ? 'cheap-inline-module-source-map' : false,

  plugins: [
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.DefinePlugin({
      NODE_ENV: JSON.stringify(NODE_ENV),
      LANG: JSON.stringify('ru'),
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'common',
      minChunks: 2,
    }),
  ],

  module: {
      loaders: [
          {
              test: /\.js$/,
              loader: 'babel-loader',
              exclude: /node_modules/,
              query: {
                  presets: ['es2015'],
                  plugins: ['transform-runtime']
              }
          }
      ]
  }
};

if (NODE_ENV == 'production') {
  module.exports.plugins.push(
    new webpack.optimize.UglifyJsPlugin({
      comporess: {
        warnings: false,
        drop_console: true,
        unsafe: true,
      }
    })
  );
}
