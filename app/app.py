from multiprocessing import Process
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler, PatternMatchingEventHandler
import time


from sanic import Sanic
from sanic.views import HTTPMethodView
from sanic.response import text, html
import websockets
import asyncio

from jinja2 import Environment, PackageLoader, select_autoescape

from config.settings import base as settings


jinja_env = Environment(
    loader=PackageLoader('app', 'templates'),
    autoescape=select_autoescape(['html', 'xml'])
)


class Reloader:

    class Handler(PatternMatchingEventHandler):
        patterns = ["*.py", "*.html"]
        ignore_patterns = ["*.scss"]
        ignore_directories = False
        case_sensitive = False

        def __init__(self, reloader):
            self.reloader = reloader

        def on_any_event(self, event):
            self.reloader.reload()

    def __init__(self, directory, callback):
        self.observer = None
        self.process = None
        self.handler = Reloader.Handler(self)
        self.directory = directory
        self.callback = callback

    def watch(self, first=True):
        if first:
            self.reload()
        else:
            self.observer = Observer()
            self.observer.schedule(
                self.handler, self.directory, recursive=True)
            self.observer.start()

    def reload(self):
        if self.observer:
            self.observer.stop()
            self.process.terminate()
            self.process.join()
        try:
            self.process = Process(target=self.callback)
            self.process.start()
        finally:
            self.watch(first=False)


class WebSocketSanic(Sanic):
    def __init__(self):
        super().__init__()
        self.before_start = None

    def websocket(self, handler, host=None, port=None,
                  *args, **kwargs):
        if kwargs.get('host') is None:
            kwargs['host'] = 'localhost'
        if kwargs.get('port') is None:
            kwargs['port'] = '3000'
        server = websockets.serve(handler, *args, **kwargs)

        @self.listener('before_server_start')
        def before_start(app, loop):
            asyncio.get_event_loop().run_until_complete(server)


class ContextMixin(object):
    """
    A default context mixin that passes the keyword arguments received by
    get_context_data as the template context.
    """

    def get_context_data(self, **kwargs):
        if 'view' not in kwargs:
            kwargs['view'] = self
        return kwargs


class TemplateResponseMixin(object):
    """
    A mixin that can be used to render a template.
    """
    template_name = None

    def render_to_response(self, context, **response_kwargs):
        """
        Returns a response,
        """
        response_kwargs.setdefault('content_type', self.content_type)
        return html(self.template.render(**context))

    def get_template_name(self):
        """
        Returns a list of template names to be used for the request. Must return
        a list. May not be called if render_to_response is overridden.
        """
        if self.template_name is None:
            raise ImproperlyConfigured(
                "TemplateResponseMixin requires either a definition of "
                "'template_name' or an implementation of 'get_template_name()'")
        else:
            return [self.template_name]


class TemplateView(TemplateResponseMixin, ContextMixin, HTTPMethodView):
    """
    A view that renders a template. 
    """
    async def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)


class SimpleView(HTTPMethodView):

    template = jinja_env.get_template('base.html')

    async def get(self, request):
        context = {'name': 'Hagen', }
        return html(self.template.render(**context))


def run_server(
        host=settings.SANIC_HOST,
        port=settings.SANIC_PORT,
        debug=settings.DEBUG):
    app = Sanic(__name__)
    app.static('/static', settings.STATIC_ROOT)
    app.add_route(SimpleView.as_view(), '/')
    app.run(host, port, debug)


if __name__ == '__main__':
    if settings.DEBUG:
        Reloader(settings.ROOT_DIR, run_server).watch()
        while True:
            time.sleep(1)
    else:
        run_server()
